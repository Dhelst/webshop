const userBalance = document.getElementById("userbalance");
const loanButton = document.getElementById("loan");
const payElement = document.getElementById("pay");
const bankButton = document.getElementById("bank");
const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");
const priceElement = document.getElementById("price");
const buyNowElement = document.getElementById("buynow")
const imageElement = document.getElementById("image")
const cartelement = document.getElementById("cart")
const descriptionElement = document.getElementById("description")
const repayButtonElement = document.getElementById("repay")
const userLoanDivElement = document.getElementById("userloan")
const workButtonElement = document.getElementById("work")

let computers = [];
let cart = [];
let price = 0.0;


let fetchedComputers = []


let income = 0;

function workAdd() {
    income += 100;
    payElement.innerHTML = income + " NOK";
}



const onFetch = (computers) => {
    fetchedComputers = computers
    addComputerToMenu(computers);

    const selected = fetchedComputers.find(computer => computer.id === 1)

    const featureOption = document.createElement("dd");
    featureOption.textContent = selected.description;
    featuresElement.replaceChildren(featureOption)

}

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => onFetch(computers));




let credit = 200;
let loan = 0;
userBalance.innerHTML = credit.toLocaleString("no-NO", {
    style: "currency",
    currency: "Nok",
});

let gotLoan = false;

function takeLoan() {
    let amount = parseInt(prompt("Enter an amount:"), 0);

    amount = Number(amount);
    let possibleLoan = credit * 2;
    if (!isNaN(amount) && amount <= possibleLoan && !gotLoan) {
        credit += amount;
        loan += amount;
        gotLoan = true;

        userBalance.innerHTML = credit.toLocaleString("no-No", {
            style: "currency",
            currency: "Nok",
        });
        repayButtonElement.removeAttribute("hidden");
        userLoanDivElement.style = "display: static";
        userLoanDivElement.innerText = loan.toLocaleString("no-No", {
            style: "currency",
            currency: "Nok",
        });
    } else if (gotLoan) {
        alert("You already have a loan");
    }
}

function loanpayment() {
    if (income >= loan);
    userLoanDivElement.innerText = "";
    income = income - loan;
    gotLoan = false;
    loan = 0;
    userBalance.innerHTML = income + " NOK";
    userLoanDivElement.style = "display: none";

}





computersElement.addEventListener("change", (e) => {
    const computerId = parseInt(e.target.value)

    const selected = fetchedComputers.find(computer => computer.id === computerId)
    const description = selected.description
    const featureOption = document.createElement("dd");
    featureOption.textContent = description;
    featuresElement.replaceChildren(featureOption)

})


const addComputerToMenu = (computer) => {
    computer.forEach(computer => {
        const computerOption = document.createElement("option");
        computerOption.value = computer.id;
        computerOption.innerText = computer.title;
        computersElement.appendChild(computerOption);
        priceElement.innerText = computers[0].price;
    })
}

const addFeaturesToMenu = (features) => {
    features.forEach(x => addFeature(x));

}

const addFeature = (feature) => {
    feature.forEach(feature => {
        featureOption.value = feature.id;
        featureOption.appendChild(document.createTextNode(feature.description));
        featuresElement.appendChild(featureOption);
    })
}
//price to priceElement
const computerPrice = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    priceElement.innerText = selectedComputer.price.toLocaleString('nb-NO', { style: 'currency', currency: 'NOK' });

}

computersElement.addEventListener("change", computerPrice);

const imageList = (item) => {
    const selectedComputer = computers[item.target.selectedIndex];
    picture.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`;
    selectedComputer.innerHTML = picture.src;
}
computersElement.addEventListener("change", imageList)


const computerDescription = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    descriptionElement.innerText = selectedComputer.specs;
}

function buyIt() {
    credit = credit;
    let price = parseInt(priceElement.innerText);
    if (price > credit) {
        alert("not enough funds. ");
    } else {
        credit -= price;
        document.getElementById("userbalance").innerHTML = credit.toLocaleString(
            "no-No",
            {
                style: "currency",
                currency: "Nok",
            }
        );

        alert("congratulations! you own a new computer. ");
    }
}

computersElement.addEventListener("change", computerDescription);


loanButton.addEventListener("click", takeLoan);


buyNowElement.addEventListener("click", buyIt);

workButtonElement.addEventListener("click", workAdd);

repayButtonElement.addEventListener("click", loanpayment);